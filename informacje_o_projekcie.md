# Strona nieistniejącego startupu

## Dla użytkownika

- Stworzyć początkową stronę z opisem startupu (1) DONE
- Stworzyć formulaż zapisujący ludzi na wersję beta (1) DONE
- Stworzyć formę na newsletter (1) DONE
- Użyć scroll-snap (2) DONE
- Strona powinna oferować możliwość zalogowania się dla użytkownika (2) DONE
- Strona powinna pokazać, ile zostało wolnych miejsc (3) DONE
- Strona powinna oferować opcję kupienia miejsca na liście (3) DONE
- Użytkownik powinien zobaczyć swoje miejsce na liście (3) DONE

## Dla administratora

- Administrator może zobaczyć całą listę (1) DONE
- Administrator może usuwać ludzi z listy i zarządzać wpisami (2) (Niemożliwe ze względu na brak backendu)
- Strona powinna oferować możliwość zalogowania się dla administratora (2) (niemożliwe ze względu na brak backendu (normalnie administrator logowałby się tam, gdzie użytkownik).
- Administrator może zmienić zawartość newslettera (3) (Niemożliwe ze względu na brak backendu)
