var maxPeople = 1000;
var currentNumber = 0;
var faker = require('faker');
var randomvalue = Math.floor(Math.random()*1000);

function createTable(tableData) {
	var table = document.createElement('table');
	var tableBody = document.createElement('tbody');

	tableData.forEach(function(rowData) {
		var row = document.createElement('tr');

		rowData.forEach(function(cellData) {
			var cell = document.createElement('td');
			cell.appendChild(document.createTextNode(cellData));
			row.appendChild(cell);
		});

		tableBody.appendChild(row);
	});

	table.className = "table";
	table.appendChild(tableBody);
	document.querySelector("#SomeTable").appendChild(table);
}


// create a function that generates an array of a random name, random surname, random number

function generateNames() {
	usersArray = [];
	usersArray.push(faker.name.findName());
	usersArray.push(faker.internet.email());
	return usersArray;
}

// connect a random number of those arrays together

function connectArrays() {
	var connectedArray = [];
	for (var i = 0; i < 1000 - randomvalue; i++) {
		toModifyArray = generateNames();
		toModifyArray.unshift(i + 1);
		connectedArray.push(toModifyArray);
	}
	return connectedArray;
}

function changeNumber() {
	var info = document.querySelector('#info');
	var taken = 1000 - randomvalue;
	info.innerHTML = "Places taken: " + taken  + " out of 1000.";
}

createTable(connectArrays());
changeNumber();
