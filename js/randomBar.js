bar = document.querySelector("#progress-bar");
places = document.querySelector("#places");
randomvalue = Math.floor((Math.random() * 1000) + 1);
bar.style.width=randomvalue/10 + '%';
bar.setAttribute("aria-valuenow", randomvalue);
placesLeft = 1000 - randomvalue;
places.innerText = "Places in beta left: " + placesLeft + " out of 1000.";
info = document.querySelector("#info");
info.innerText = "You are the " + randomvalue + " person out of 1000!";
